package main

import (
	"fmt"
	"os"
	"os/exec"
	"testing"
)

func TestNoArgsCall(t *testing.T) {

	in := "samples/in/toclean"
	out := "/tmp/anyfmt_test_output1"
	expected := "samples/out/clean"

	cmd := fmt.Sprintf("cat %s | anyfmt > %s; diff %s %s", in, out, expected, out)
	err := exec.Command("bash", "-c", cmd).Run()
	if err != nil {
		t.Fatalf("Expected output differs from actual output.\nFor more details run diff %v %v", expected, out)
	}
}

func TestDefaulFormatting(t *testing.T) {

	in := "samples/in/toclean"
	out := "/tmp/anyfmt_test_output2"

	os.Args = []string{"anyfmt", "-in=" + in, "-out=" + out}
	main()
	expected := "samples/out/clean"
	err := exec.Command("diff", expected, out).Run()
	if err != nil {
		t.Fatalf("Expected output differs from actual output.\nFor more details run diff %v %v", expected, out)
	}
}
