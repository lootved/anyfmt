package format

import (
	"bufio"
	"io"
	"strings"

	. "gitlab.com/lootved/anyfmt/config"
)

func Basic(fi io.Reader, fo io.Writer) {

	reader := bufio.NewScanner(fi)
	indentLvl := 0
	emptyLinesSoFar := 0
	prefix := ""
	suffix := ""
	prefixs := []string{")", "}", "]"}
	suffixs := []string{"(", "{", "["}

	for reader.Scan() {
		line := reader.Text()
		Debuglog(line, indentLvl)
		line = strings.TrimSpace(line)
		if line == "" {
			if emptyLinesSoFar < Cfg.NbrEmptyLines {
				fo.Write([]byte("\n"))
			}
			emptyLinesSoFar++
			continue
		}
		emptyLinesSoFar = 0
		prefix = line[:1]
		suffix = line[len(line)-1:]

		if contains(prefixs, prefix) && indentLvl > 0 {
			indentLvl = indentLvl - 1
		}

		line = indent(line, indentLvl)
		fo.Write([]byte(line + "\n"))

		if contains(suffixs, suffix) {
			indentLvl = indentLvl + 1
		}
	}
}

func indent(line string, level int) string {
	if level > 0 {
		line = strings.Repeat(Cfg.IndStr, level*Cfg.Ilen) + line
	}
	return line
}

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}
