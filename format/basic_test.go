package format

import (
	"bytes"
	"strings"
	"testing"

	. "gitlab.com/lootved/anyfmt/config"
)

func TestBasicFormat(t *testing.T) {
	Cfg.Ilen = 2
	Cfg.IndStr = " "

	input := `a{
a
[a]
[
a
    ]


 a 
}
last empty line stays
    `
	r := strings.NewReader(input)
	var b bytes.Buffer
	Basic(r, &b)
	expected := `a{
  a
  [a]
  [
    a
  ]
  a
}
last empty line stays
`
	if expected != b.String() {
		t.Fatal("Basic formatting failed")
	}
}
