package format

import (
	"bytes"
	"log"
	"strings"
	"testing"

	. "gitlab.com/lootved/anyfmt/config"
)

func TestJsonFormat(t *testing.T) {
	Cfg.Ilen = 2
	Cfg.IndStr = " "
	Cfg.Mode = "auto"

	r := strings.NewReader(input)
	var b bytes.Buffer
	Json(r, &b)
	expected := `{
  "hello": "world",
  "json": {
    "a": "b"
  },
  "to": [
    "format",
    "this"
  ]
}`
	if expected != b.String() {
		log.Println(expected, "\n", b.String())
		t.Fatal("Json formatting failed for multiline")
	}
}

func TestJsonFormatOneLine(t *testing.T) {
	Cfg.Ilen = 0
	Cfg.IndStr = " "
	Cfg.Mode = "auto"

	r := strings.NewReader(input)
	var b bytes.Buffer
	Json(r, &b)
	expected := `{"hello":"world","json":{"a":"b"},"to":["format","this"]}`
	if expected != b.String() {
		log.Println(expected, "\n", b.String())
		t.Fatal("Json formatting failed for one liner")
	}
}

var input = `
{"hello":"world", "to":["format", "this"],
    "json":{"a":"b"}

}
  `
