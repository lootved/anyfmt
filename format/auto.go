package format

import (
	"io"
	"log"
	"strings"

	. "gitlab.com/lootved/anyfmt/config"
)

func Auto(fi io.Reader, fo io.Writer) {
	switch strings.ToUpper(Cfg.Filetype) {
	case JSON:
		Json(fi, fo)
	default:
		log.Fatalln("unsupported filetype")
	}

}

const (
	JSON = "JSON"
)
