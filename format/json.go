package format

import (
	"encoding/json"
	"io"
	"log"

	. "gitlab.com/lootved/anyfmt/config"
)

func Json(fi io.Reader, fo io.Writer) {
	b, err := io.ReadAll(fi)
	//log.Println("read:", string(b))
	if err != nil {
		log.Fatalln("unable to read json input", err)
	}
	var m interface{}
	json.Unmarshal(b, &m)
	res := jmarshall(m)
	//	log.Println("written", string(res))
	fo.Write(res)
}

func jmarshall(m interface{}) []byte {
	var res []byte
	var err error
	//log.Println("indenting with", Cfg.Ilen/2, "characters")
	if Cfg.Ilen == 0 {
		res, err = json.Marshal(m)
	} else {
		res, err = json.MarshalIndent(m, "", indent("", Cfg.Ilen/2))
	}
	if err != nil {
		log.Fatalln("unable to marshal content of input file", err)
	}
	return res
}
