package config

import "log"

var Cfg struct {
	OutputPath    string
	InputPath     string
	IndStr        string
	Mode          string
	Filetype      string
	InPlace       bool
	Debug         bool
	Ilen          int
	NbrEmptyLines int
}

func Debuglog(s ...any) {
	if Cfg.Debug {
		log.Println(s...)
	}
}
