# AnyFmt

## Overview

Minimal code formatter designed to beautify code where blocks are delimited by {}.

Works well enough on C, C++, bash, groovy, javascript.

Can format json to one line if `ilen == 0` or multiple indented lines otherwise.

Mainly used to quickly format code in `Neovim` without having to install
a full lsp or when the lsp/formatter is bugged.

## Usage

```
anyfmt -h
Usage of anyfmt:
  -d	enable debug by logging to file
  -dir string
    	folder where to store temporary files and logs (default "/tmp/anyfmt")
  -ilen int
    	indentaton size (default 2)
  -in string
    	input path to file to process, defaults to stdin
  -ind string
    	indentation symbol, should be 1 char (default " ")
  -inplace
    	save result in input path
  -n int
    	number of empty lines allowed (default 2)
  -out string
    	ouput path where to save the result, defaults to stdout
```


## Formatting style
Formatting configuration is limited to the `indentation symbol`,
the `size of an indentation` and the `maximum number of consecutive empty lines`

The output of the default options can be seen here:
![before](samples/in/toclean) and ![after](samples/out/clean)
