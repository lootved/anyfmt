package main

import (
	"log"
	"os"

	. "gitlab.com/lootved/anyfmt/config"
	"gitlab.com/lootved/anyfmt/format"
)

func runFormat() {
	switch Cfg.Mode {
	case basic:
		format.Basic(fi, fo)
	case auto:
		format.Auto(fi, fo)
	}
}

func main() {
	parseArgs()
	runFormat()
	moveFileIfNeeded()
}

func moveFileIfNeeded() {
	if Cfg.InPlace {
		fo.Close()
		fi.Close()
		err := os.Rename(Cfg.OutputPath, Cfg.InputPath)
		if err != nil {
			log.Fatalln(err)
		}
	}
}
