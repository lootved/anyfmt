package main

import (
	"flag"
	"log"
	"os"
	"path/filepath"

	. "gitlab.com/lootved/anyfmt/config"
)

var (
	fi = os.Stdin
	fo = os.Stdout
)

const (
	basic = "basic"
	auto  = "auto"
)

func parseArgs() {
	var workDir string
	flag.StringVar(&workDir, "dir", "/tmp/anyfmt", "folder where to store temporary files and logs")
	flag.StringVar(&Cfg.OutputPath, "out", "", "ouput path where to save the result, defaults to stdout")
	flag.StringVar(&Cfg.InputPath, "in", "", "input path to file to process, defaults to stdin")
	flag.StringVar(&Cfg.IndStr, "ind", " ", "indentation symbol, should be 1 char")
	flag.StringVar(&Cfg.Mode, "mode", "basic", `"basic": very basic, fast language agnosnic formating,
    only works when blocks are delimited by (), [] or [] chars.
"auto": will try to apply more advanced formatting, fails on syntax errors`)

	flag.StringVar(&Cfg.Filetype, "filetype", "auto", `defines the file type in "auto" mode.
  "auto": will extract filetype from extension.`)

	flag.BoolVar(&Cfg.InPlace, "inplace", false, "save result in input path")
	flag.BoolVar(&Cfg.Debug, "d", false, "enable debug by logging to file")

	flag.IntVar(&Cfg.Ilen, "ilen", 2, "indentaton size")
	flag.IntVar(&Cfg.NbrEmptyLines, "n", 2, "number of empty lines allowed")

	flag.Parse()

	validateFlags()

	var err error

	// fi and fo must not be closed
	// will be freed when the program exists anyway

	// not a big deal to ignore errors as next commands will exit on error
	os.MkdirAll(workDir, os.ModePerm)

	if Cfg.InputPath != "" {
		fi, err = os.Open(Cfg.InputPath)
		if err != nil {
			log.Fatalln(err)
		}
		if Cfg.OutputPath == Cfg.InputPath {
			Cfg.InPlace = true
		}
	}
	if Cfg.InPlace {
		Cfg.OutputPath = Cfg.InputPath + ".tmp"
	}

	if Cfg.OutputPath != "" {
		fo, err = os.Create(Cfg.OutputPath)
		if err != nil {
			log.Fatalln(err)
		}
	}

	if Cfg.Filetype == auto {
		Cfg.Filetype = filepath.Ext(Cfg.InputPath)
		if len(Cfg.Filetype) > 0 {
			Cfg.Filetype = Cfg.Filetype[1:]
		}
	}
	if Cfg.Debug {
		logToFile(workDir + "/fmt.log")
		log.Printf("%+v\n", Cfg)
	}
}

func validateFlags() {
	switch Cfg.Mode {
	case basic, auto:
		// do nothing
	default:
		log.Fatalln("invalid mode , -h to display usage")
	}
}

func logToFile(path string) {
	f, err := os.Create(path)
	if err != nil {
		log.Println("error opening log file ", path, err)
		log.Println("logs will not be persisted")
		return
	}
	log.SetOutput(f)
	log.Println("Started formatter")
}
